import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

public class TranverseToGiants extends Task {

    @Override
    public boolean validate() {
        return ReadyToLeave();
    }

    @Override
    public int execute() {

        WalkToGiants();
        return 330;
    }


    private boolean ReadyToLeave() {
        if (Inventory.contains(Main.FoodItem) && Main.FoodSelected() && !Main.Hill_Giant_Area.contains(Players.getLocal())) {
            return true;
        } else {
            return false;
        }
    }

    private void WalkToGiants() {
        //while not at ladder, walk there

        if (Players.getLocal().getPosition().getY() < 9000) {//If in overworld
            if (!Main.Edgeville_ladder_position.contains(Players.getLocal())) {//if not by ladder
                Movement.walkTo(Main.Edgeville_ladder_position.getCenter());
                Time.sleep(Random.nextInt(2000, 2500));
            }
            if (Main.Edgeville_ladder_position.contains(Players.getLocal())) {
                SceneObject trapdoor = SceneObjects.getNearest("Trapdoor");
                trapdoor.click();
                Time.sleep(Random.nextInt(1000, 1700));
                SceneObjects.getNearest("Trapdoor").interact("Climb-down");
            }

        }
        if (Players.getLocal().getPosition().getY() > 9000 && !Main.Hill_Giant_Area.contains(Players.getLocal())) {
            Movement.walkTo(Main.Hill_Giant_Area.getCenter());
            Main.TestText = "Walking to Giants";
            Time.sleep(Random.nextInt(900, 1500));
        }
    }
}
