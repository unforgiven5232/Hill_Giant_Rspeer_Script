import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BattleGUI extends JFrame {
    private JComboBox FoodDropDown;
    private JButton StartScript;
    String[] FoodList = {"","Lobster", "Salmon","Trout","Tuna","Swordfish"};

    public BattleGUI(){
        super("Oswalds Hill Giant Fighter");
        setLayout(new FlowLayout());
        this.setSize(250,100);
        StartScript = new JButton("Set Food");
        FoodDropDown = new JComboBox(FoodList);
        StartScript.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent actionEvent) {
              Main.FoodItem = FoodDropDown.getSelectedItem().toString();
          }
      });
                add(FoodDropDown);
        add(StartScript);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setAlwaysOnTop(true);
    }



}
