import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;

import java.awt.*;


@ScriptMeta(name = "HillGiant Fighter", desc = "Fights Hill Giants and stores their drops for you. Start bot in Edgeville bank", developer = "Oswald", category = ScriptCategory.COMBAT)
public class Main extends TaskScript implements RenderListener {
    public static final Area Hill_Giant_Area = Area.polygonal(
            new Position(3108, 9823, 0),
            new Position(3102, 9825, 0),
            new Position(3098, 9828, 0),
            new Position(3096, 9831, 0),
            new Position(3096, 9834, 0),
            new Position(3097, 9836, 0),
            new Position(3099, 9837, 0),
            new Position(3101, 9838, 0),
            new Position(3105, 9838, 0),
            new Position(3109, 9844, 0),
            new Position(3109, 9847, 0),
            new Position(3110, 9849, 0),
            new Position(3114, 9850, 0),
            new Position(3123, 9850, 0),
            new Position(3124, 9849, 0),
            new Position(3124, 9846, 0),
            new Position(3125, 9845, 0),
            new Position(3124, 9843, 0),
            new Position(3122, 9840, 0),
            new Position(3123, 9838, 0),
            new Position(3123, 9836, 0),
            new Position(3122, 9834, 0),
            new Position(3119, 9831, 0),
            new Position(3117, 9829, 0),
            new Position(3114, 9828, 0),
            new Position(3112, 9828, 0),
            new Position(3111, 9826, 0),
            new Position(3110, 9824, 0));
    public static final Area Edgeville_bank = Area.rectangular(3092, 3489, 3094, 3495);
    public static final Area Edgeville_ladder_position = Area.rectangular(3093, 3468, 3097, 3473);
    public static final Area Edgeville_ladder_position_underground = Area.rectangular(3096, 9867, 3098, 9870);
    public static String FoodItem;
    public static String TestText = "";

    private static final Task[] TASKS = {new BankItems(), new TranverseToBank(), new TranverseToGiants(), new FightGiants()};

    @Override
    public void onStart() {
        FoodItem = null;
        new BattleGUI().setVisible(true);
        submit(TASKS);
    }

    @Override
    public void onStop() {

    }

    @Override
    public void notify(RenderEvent re) {
        Graphics g = re.getSource();
        g.setColor(Color.RED);

        g.drawString(TestText, 30, 300);

    }

    public static boolean FoodSelected() {
        if (FoodItem != null && FoodItem != "") {
            return true;
        } else {
            return false;
        }
    }

}
